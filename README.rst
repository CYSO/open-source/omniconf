omniconf
========

.. image:: https://img.shields.io/gitlab/pipeline-status/CYSO%2Fopen-source%2Fomniconf?branch=develop&logo=gitlab
   :alt: Gitlab Pipeline Status

.. image:: https://img.shields.io/gitlab/pipeline-coverage/CYSO%2Fopen-source%2Fomniconf?branch=develop&logo=gitlab
   :alt: Gitlab Code Coverage

.. image:: https://img.shields.io/pypi/l/omniconf
   :alt: License
   :target: https://pypi.python.org/pypi/omniconf

.. image:: https://img.shields.io/pypi/v/omniconf
   :alt: PyPI version
   :target: https://pypi.python.org/pypi/omniconf

.. image:: https://img.shields.io/pypi/pyversions/omniconf
   :alt: Supported Python versions
   :target: https://pypi.python.org/pypi/omniconf

.. image:: https://img.shields.io/pypi/implementation/omniconf
   :alt: Supported Python implementations
   :target: https://pypi.python.org/pypi/omniconf

A Python library that makes configuring your application independent from your configuration backend.

Documentation
-------------

Read the complete documentation on `Read the Docs <http://omniconf.readthedocs.io/en/latest/>`_.

Why omniconf?
-------------

Configuring applications is hard, and it doesn't help that there are many different (and valid) ways to do it:

* cli arguments
* config files: ConfigObj (.ini like), JSON, YAML, TOML
* environment variables

Each of this methods are a valid way to configure an application, and each have their own strengths. Cli arguments are
most suited for tools and daemons. Configuration files are suited for applications that have more complex requirements.
Environment variables and key/value stores are handy when using containers. You may even want to use a combination of
methods (not yet implemented).

This library aims to make configuring the application easier, and allows you to use multiple configuration backends
transparently.

For up-to-date examples, take a look `here <http://omniconf.readthedocs.io/en/latest/examples.html>`_.

Changes
-------

For an up-to-date changelog, see `ChangeLog`_.

.. _ChangeLog: ChangeLog

* Full typing support and experimental mypy plugin added in 1.5.0 .
* Support for Python 3.9, 3.10, 3.11 and 3.12 was added in version 1.5.0 .
* Support for Python 2.7, 3.5, 3.6 and 3.7 was dropped in version 1.5.0 .
* Support for Vault backend was dropped in version 1.5.0 .
* Support for Python 3.4 was dropped in version 1.4.0 .
* Support for Jython was dropped in version 1.4.0 .
* Support for Python 3.8 was added in version 1.3.1 .
* Support for Python 3.3 was dropped in version 1.3.0 .

License
-------

omniconf is licensed under LGPLv3. See the LICENSE file for details.


Contributing
------------

To contribute, base your changes on the develop branch. Use `init.sh` to install the required Python version using `pyenv` and
setup a virtualenv in `vendor/`. Activate the virtualenv using `source vendor/bin/activate`. After the initial setup, use
`init.sh` at any time to bring your environment up-to-date with the latest requirements.

.. code-block:: bash

   $ ./init.sh
   Installing Python version 3.x.x using pyenv
   Creating Python venv named 'vendor'
   - Install uv
   - Updating pip, setuptools
   Resolved 2 packages in 2ms
   Installed 1 package in 10ms
   ...
   - Installing dev and docs dependencies
   Installed 56 packages in 32ms
   ...
   - Installing app in edit mode
   ...
   Installed 1 package in 0ms
   ...
   - Installing pre-commit hook
   pre-commit installed at .git/hooks/pre-commit

Make sure your contribution doesn't break any existing tests, and add relevant new tests. You can run the test suite using tox:

.. code-block:: bash

   $ tox

To check for style issues, just run the test suite, ruff is automatically called for linting and formatting checks.

When you're done, open a pull request on Github.
